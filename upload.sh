#!/bin/sh
USERNAME=pi
PASSWORD=raspberry
REMOTE_PATH=/home/pi/pumpkin-supmcu-remote-master
LOCAL_PATH=../pumpkin-supmcu-remote-master
ENDPOINT=pi@10.30.3.10

sshpass -p $PASSWORD ssh $ENDPOINT "rm -rf $REMOTE_PATH"
sshpass -p $PASSWORD scp -r $LOCAL_PATH $ENDPOINT:$REMOTE_PATH
sshpass -p $PASSWORD ssh $ENDPOINT "cd $REMOTE_PATH;python setup.py install"