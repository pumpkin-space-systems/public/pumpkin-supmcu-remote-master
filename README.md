# pumpkin-supmcu-kubos Package

The `pumpkin-supmcu-kubos` package has the following functionality:

* Leverages business logic found in [pumpkin-supmcu](https://pumpkin-supmcu.readthedocs.io/en/latest/index.html).
    + Please read `pumpkin-supmcu` documentation for usage, this is the implementation package for [kubos-i2c-hal](https://docs.kubos.com/1.21.0/deep-dive/apis/kubos-hal/i2c-hal/python-i2c.html)
* Interfaces `pumpkin-supmcu` package with the KubOS I2C HAL library.

The documentation for the `pumpkin_supmcu` package can be [found here](https://pumpkin-supmcu.readthedocs.io/en/latest/index.html).
