# coding: utf-8
# ##############################################################################
#  (C) Copyright 2019 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Implementation of remote i2c client
"""
import socket

from typing import List
from pumpkin_supmcu.i2c import I2CMaster, I2CBusSpeed

from .types import MessageType, WriteRequest, ReadRequest, Message, deserialize_response
from .parse import read_socket, send_data


class I2CRemoteClient:
    """
    Implementation of I2C remote client, creates connection and communicates with server
    """
    def __init__(self, ip: str, port: int, sync_sequence: bytes):
        """
        Creates an I2C client and connects to a server socket

        :param ip: The IP address of the server
        :param port: The port to connect to on the server
        :param sync_sequence: A byte sequence used to define the end of each message
        """
        self.sock = socket.create_connection((ip, port))
        self.sync_sequence = sync_sequence
        self.sock.setblocking(True)

    def read_from(self) -> Message:
        """
        Uses the read_socket function in parse.py to load bytes into the buffer until the sync sequence is found

        :return: Response Message read from socket
        """
        response_dict = read_socket(self.sock, self.sync_sequence)
        return deserialize_response(response_dict)

    def send_request(self, request: Message) -> Message:
        """
        Uses send_data from parse.py to compile the request into a byte array and send it

        :param request: A Message object that contains the request
        """
        send_data(self.sock, self.sync_sequence, request)
        return self.read_from()


class I2CRemoteClientMaster(I2CMaster):
    """
    Implementation of I2CMaster for remote client
    """
    def __init__(self, ip_addr: str, port=7800, sync_sequence=b'\xab\xcd\xef'):
        """
        Creates an :class:`~pumpkin_supmcu.I2CMaster` using kubos's i2c interface

        :param ip_addr: The IP address of the server
        :param port: The port on the server to connect to
        :param sync_sequence: A byte sequence used to define the end of each message
        """
        self.client = I2CRemoteClient(ip_addr, port, sync_sequence)
        self.ip_addr = ip_addr
        self.status = 0

    @property
    def device_name(self) -> str:
        """Gets the device's hostname"""
        return "remote"

    # FIXME this should return the current baudrate, not just the default
    @property
    def device_speed(self) -> I2CBusSpeed:
        """The default I2C baudrate"""
        return I2CBusSpeed(100)

    @device_speed.setter
    def device_speed(self, bus_speed: I2CBusSpeed):
        """"Supposed to set the baudrate of the I2C bus"""
        raise NotImplementedError("The I2C baud rate cannot be changed without rebooting")

    @property
    def device_pullups(self) -> bool:
        """If the I2C SDA/SCL pullups are ON or OFF."""
        return True

    @device_pullups.setter
    def device_pullups(self, is_on: bool):
        """Supposed to set the state of the I2C SDA/SCL pullups ON or OFF."""
        raise NotImplementedError("The pullups cannot be changed in software")

    def write(self, addr: int, b: bytes):
        """
        Writes all of `b` bytes to address `addr`

        :param addr:  The I2C Address to write to.
        :param b: The bytes `b` to write to the I2C Bus.
        """
        response = self.client.send_request(Message(MessageType.Write, WriteRequest(addr, b)))
        if response.message_type != MessageType.Write:
            raise RuntimeError(f"Expected write, got {response.message_type}")

    def read(self, addr: int, amount: int) -> bytes:
        """
        Reads `amount` bytes of data from address `addr`

        :param addr: The I2C Address to read from.
        :param amount: The amount of bytes to read from the bus.
        :return: The bytes read from the bus.
        """
        response = self.client.send_request(Message(MessageType.Read, ReadRequest(addr, amount)))
        if response.message_type != MessageType.Read:
            raise RuntimeError(f"Expected read, got {response.message_type}")
        return response.data.data

    def get_bus_devices(self) -> List[int]:
        """
        Gets the available I2C devices from the selected I2C bus and
        returns a list of device addresses

        :return: A list of device addresses
        """
        response = self.client.send_request(Message(MessageType.Bus, None))
        if response.message_type != MessageType.Bus:
            raise RuntimeError(f"Expected bus, got {response.message_type}")
        return response.data.devices
