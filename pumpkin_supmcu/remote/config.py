"""
Loads in config toml file to run server
"""
from pathlib import Path
import toml


def load_config(p: Path):
    """Loads in the configuration TOML file at the path `p`."""
    with p.open("r") as f:
        conf = toml.load(f)
    return conf
