"""
Utility functions to read socket and send data
"""
import sys
import socket
import pickle

from dataclasses import asdict
from .types import Message


def read_socket(sock: socket, sync_sequence: bytes) -> Message:
    """
    Reads from a socket until the sync_sequence is found, signaling the end of the message

    :param sock: Socket to read from
    :param sync_sequence: Sync sequence at the end of the message
    :param direction: "request" or "response"
    :return: Message object containing the message received
    """
    #print(f"read_socket was called with parameters {sock}, {sync_sequence}, {direction}")
    buffer = bytearray(0)
    sync_found = -1
    sync_size = sys.getsizeof(sync_sequence)
    # buffer.find returns -1 when the sequence is not found
    while sync_found == -1:
        #print("Receiving...")
        buffer += sock.recv(64)
        sync_found = buffer.find(sync_sequence)
        if not buffer:
            return buffer
        # Once sequence is found, cuts byte array to end after the sync sequence
        if sync_found != -1:
            total_len = sync_found + sync_size
            buffer = buffer[0:total_len]
    buf_str = buffer[:sync_found]
    #print(buf_str)
    return pickle.loads(buf_str)


def send_data(sock: socket, sync_sequence: bytes, message: Message):
    """
    Formats request or response into a byte array and sends it

    :param sock: Socket that is sending the message
    :param sync_sequence: Sync sequence to append to the end of each message
    :param message: Message to be sent over the socket
    """
    #print(f"send_data was called with parameters {sock}, {sync_sequence}, {message}")
    message_dict = asdict(message)
    pickle_str = pickle.dumps(message_dict)
    #print(message)
    #print(message_dict)
    #print(pickle_str)
    sock.sendall(pickle_str+sync_sequence)
