# coding: utf-8
# ##############################################################################
#  (C) Copyright 2019 Pumpkin, Inc. All Rights Reserved.                       #
#                                                                              #
#  This file may be distributed under the terms of the License                 #
#  Agreement provided with this software.                                      #
#                                                                              #
#  THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND,                   #
#  INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND                       #
#  FITNESS FOR A PARTICULAR PURPOSE.                                           #
# ##############################################################################
"""
Implements remote i2c server
"""
import sys
import socket

from typing import List
from pathlib import Path
from subprocess import run
from socket import gethostname
from pumpkin_supmcu.i2c import I2CMaster, I2CBusSpeed

from .types import MessageType, WriteResponse, ReadResponse, BusResponse, FailResponse, Message, deserialize_request
from .parse import read_socket, send_data
from .config import load_config

# Only include definition for I2CLinuxMaster on Linux machines
try:
    from i2c import I2C
except ImportError:
    I2C = None
if sys.platform.startswith('linux'):
    from plumbum import local
    from smbus2 import SMBus, i2c_msg


class I2CRemoteServerMaster(I2CMaster):
    """
    Implementation of I2CMaster to run on a remote server
    """
    def __init__(self, port: int):
        """
        Creates an :class:`~pumpkin_supmcu.I2CMaster` using kubos's i2c interface

        :param port: the number of the kubos bus that is connected to the SupMCU
        """
        self.platform = "linux" if sys.platform.startswith('linux') else "kubos"
        if self.platform == "linux":
            self.bus = SMBus(port)
        else:
            if I2C is None:
                raise NotImplementedError("I2CKubosServerMaster is not available on this system; "
                                          "is the KubOS I2C HAL package available?")
            self.bus = I2C(port)
        self.port = port
        self.status = 0

    @property
    def device_name(self) -> str:
        """Gets the device's hostname"""
        return gethostname()

    # FIXME this should return the current baudrate, not just the default
    @property
    def device_speed(self) -> I2CBusSpeed:
        """The default I2C baudrate"""
        return I2CBusSpeed(100)

    @device_speed.setter
    def device_speed(self, bus_speed: I2CBusSpeed):
        """"Supposed to set the baudrate of the I2C bus"""
        raise NotImplementedError("The I2C baud rate cannot be changed without rebooting")

    @property
    def device_pullups(self) -> bool:
        """If the I2C SDA/SCL pullups are ON or OFF."""
        return True

    @device_pullups.setter
    def device_pullups(self, is_on: bool):
        """Supposed to set the state of the I2C SDA/SCL pullups ON or OFF."""
        raise NotImplementedError("The pullups cannot be changed in software")

    def write(self, addr: int, b: bytes) -> int:
        """
        Writes all of `b` bytes to address `addr`

        :param addr:  The I2C Address to write to.
        :param b: The bytes `b` to write to the I2C Bus.
        :return: The number of bytes written
        """
        if self.platform == "linux":
            try:
                self.status = MessageType.Write
                msg = i2c_msg.write(addr, b)
                self.bus.i2c_rdwr(msg)
            except:
                self.status = MessageType.Fail
        else:
            try:
                self.status = MessageType.Write
                self.bus.write(addr, b)
            except:
                self.status = MessageType.Fail
        return sys.getsizeof(b)

    def read(self, addr: int, amount: int) -> bytes:
        """
        Reads `amount` bytes of data from address `addr`

        :param addr: The I2C Address to read from.
        :param amount: The amount of bytes to read from the bus.
        :return: The bytes read from the bus.
        """
        if self.platform == "linux":
            try:
                self.status = MessageType.Read
                msg = i2c_msg.read(addr, amount)
                self.bus.i2c_rdwr(msg)
                read_bytes = bytes(msg)
            except:
                self.status = MessageType.Fail
        else:
            try:
                self.status = MessageType.Read
                read_bytes = self.bus.read(addr, amount)
            except:
                self.status = MessageType.Fail
        return read_bytes

    def get_bus_devices(self) -> List[int]:
        """
        Gets the available I2C devices from the selected I2C bus and
        returns a list of device addresses

        :return: A list of device addresses
        """
        if self.platform == "linux":
            try:
                # plumbum.local will raise an import error if i2cdetect isn't found
                i2cdetect = local['i2cdetect']
                devices = i2cdetect["-y", str(self.port)]()
                # Parsing the output from the i2cdetect command for the device addresses
                devices = devices.split("\n")[1:]
                devices = [x.split(':')[-1].split() for x in devices]
                devices = [i for x in devices for i in x]
            except:
                self.status = MessageType.Fail
        else:
            try:
                self.status = MessageType.Bus
                # This will use subprocess run to get all devices from the I2C Bus.
                devices = str(run(['i2cdetect', '-r', str(self.port), '-y'], capture_output=True, check=True).stdout, 'ascii')
                # Parsing the output from the i2cdetect command for the device addresses
                devices = devices.split("\n")[1:]
                devices = [x.split(':')[-1].split() for x in devices]
                devices = [i for x in devices for i in x]
            except:
                self.status = MessageType.Fail
        return [int(i, 16) for i in devices if i not in ('--', 'UU')]


class I2CRemoteServer:
    """
    Class to create I2C server, handles connection requests
    """
    def __init__(self, sock: socket, port: int, i2c_port: int, sync_sequence: bytes):
        """
        Creates an I2C server on the given port and accepts a client connection

        :param sock: The socket that listens for and accepts the connection
        :param port: The port to serve the I2C interface on
        :param i2c_port: The number of the kubos bus that is connected to the SupMCU
        :param sync_sequence: A byte sequence used to define the end of each message
        """
        self.server_sock = sock
        self.sync_sequence = sync_sequence
        # Initializes I2CRemoteServerMaster on the specified port
        self.master = I2CRemoteServerMaster(i2c_port)
        # Binds socket to host address and port, listens for a connection and accepts it
        self.server_sock.bind(("0.0.0.0", port))
        self.is_connected = False

    def listen(self):
        """
        Listens for and accepts connections
        """
        self.server_sock.listen(10)
        self.sock = self.server_sock.accept()[0]
        self.sock.setblocking(True)
        self.is_connected = True

    def read_from(self) -> Message:
        """
        Uses the read_socket function in parse.py to load bytes into the buffer until the sync sequence is found

        :return: Request Message read from socket
        """
        return read_socket(self.sock, self.sync_sequence)

    def handle_request(self, request_dict: dict) -> Message:
        """
        Parses a request from an I2C client

        :param request: The Message of the request as read into the buffer
        :return: The result of attempting to carry out the request
        """
        request = deserialize_request(request_dict)
        # Attempts to write data to addr
        if request.message_type == MessageType.Write:
            amount_written = self.master.write(request.data.addr, request.data.data)
            # print(self.master.status)
            if self.master.status == MessageType.Fail:
                return Message(MessageType.Fail, FailResponse(f"Unable to write {request.data.data} to {request.data.addr}"))
            return Message(MessageType.Write, WriteResponse(amount_written))
        # Attempts to read amount bytes from addr
        if request.message_type == MessageType.Read:
            read_bytes = self.master.read(request.data.addr, request.data.amount)
            # print(self.master.status)
            if self.master.status == MessageType.Fail:
                return Message(MessageType.Fail, FailResponse(f"Unable to read {request.data.amount} bytes from {request.data.addr}"))
            return Message(MessageType.Read, ReadResponse(read_bytes))
        # Attempts to retrieve device list
        if request.message_type == MessageType.Bus:
            device_list = self.master.get_bus_devices()
            # print(self.master.status)
            if self.master.status == MessageType.Fail:
                return Message(MessageType.Fail, FailResponse("Unable to retrieve device list"))
            return Message(MessageType.Bus, BusResponse(device_list))

    def send_response(self, response: Message):
        """
        Uses send_data from parse.py to compile the response into a byte array and send it

        :param response: The returned value from handle_request, which will be sent to the client
        """
        send_data(self.sock, self.sync_sequence, response)


def execute():
    """
    Runs the I2C server
    """
    # Loads configuration, initializes server
    conf_path = " ".join(sys.argv[1:])
    config = load_config(Path(conf_path))['remote-i2c']
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print(f"Running server at port {config['port']}")
    i2c_server = I2CRemoteServer(server_socket, config['port'], config['i2c_port'], bytes(config['sync_sequence']))
    # Continuously checks for requests - if a request is found, it is parsed and responded to
    try:
        i2c_server.listen()
        print("Connection established")
        while True:
            request = i2c_server.read_from()
            if request:
                #print(f"Got: {request}")
                response = i2c_server.handle_request(request)
                #print(f"Sending: {response}")
                i2c_server.send_response(response)
            else:
                i2c_server.is_connected = False
                print("Listening")
                i2c_server.listen()
                print("Connection established")
    except OSError:
        print("Lost connection, closing socket and exiting program...")
        i2c_server.sock.close()
    finally:
        server_socket.close()
