"""
Types and utilities needed for remote i2c operations
"""
from dataclasses import dataclass
from typing import Union, Optional, List, Dict
from enum import IntEnum


class MessageType(IntEnum):
    """
    Status codes for success and failure for writing, reading, and getting list of bus devices
    """
    Write = 0
    Read = 1
    Bus = 2
    Fail = 3


@dataclass
class WriteRequest:
    """
    Write request to I2C address with data
    """
    addr: int
    data: bytes


@dataclass
class ReadRequest:
    """
    Read a certain amount of data from I2C address
    """
    addr: int
    amount: int


@dataclass
class WriteResponse:
    """
    Amount of bytes written
    """
    amount: int


@dataclass
class ReadResponse:
    """
    Data read
    """
    data: bytes


@dataclass
class BusResponse:
    """
    List of I2C device addresses
    """
    devices: List[int]


@dataclass
class FailResponse:
    """
    Message explaining failure
    """
    reason: str


MessageData = Optional[Union[WriteRequest, ReadRequest, WriteResponse, ReadResponse, BusResponse, FailResponse]]


@dataclass
class Message:
    """
    Request or response type and data
    """
    message_type: MessageType
    data: MessageData


def deserialize_request(request: Message) -> Message:
    """
    Converts JSON request into Message

    :param request: Dictionary of request containing 'message_type' and 'data'
    :return: request as a Message object
    """
    message_type = request['message_type']
    data = request['data']
    if message_type == MessageType.Write:
        return Message(message_type, WriteRequest(**data))
    if message_type == MessageType.Read:
        return Message(message_type, ReadRequest(**data))
    if message_type == MessageType.Bus:
        return Message(message_type, None)
    raise ValueError("Invalid Request Type")


def deserialize_response(response: Dict) -> Message:
    """
    Converts JSON response into Message

    :param response: Dictionary of response containing 'message_type' and 'data'
    :return: response as Message object
    """
    message_type = response['message_type']
    data = response['data']
    if message_type == MessageType.Write:
        return Message(message_type, WriteResponse(**data))
    if message_type == MessageType.Read:
        return Message(message_type, ReadResponse(**data))
    if message_type == MessageType.Bus:
        return Message(message_type, BusResponse(**data))
    if message_type == MessageType.Fail:
        return Message(message_type, FailResponse(**data))
    raise ValueError("Invalid Response Type")
